﻿namespace XVM.Extensions
{
    static class Constants
    {

        public const string DllName_X86_32 = "xvmextensions.x86_32.dll";
        public const string DllName_X86_64 = "xvmextensions.x86_64.dll";
        public const string DllName_ARM_32 = "xvmextensions.arm_32.dll";
        public const string DllName_ARM_64 = "xvmextensions.arm_64.dll";

        public const int MaxPathSize = 32767;

    }
}
