/* Copyright (c) 2017-2021, Mikhail Paulyshka.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this
*    list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
* ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#pragma once

#include "api_json.h"

#include <string>

#include <json/json.h>

#include <iostream>
#include <fstream>
#include <codecvt>

class JsonUtils
{
public:
    static Json::Value ParseString(const std::string& json);
    static Json::Value ParseWString(const std::wstring& json);

    static bool ContainsKey(const wchar_t* json, const wchar_t* path);
    static bool ContainsKey(const std::wstring&  json, const std::wstring& path);

    static std::wstring GetValue(const wchar_t* json, const wchar_t* path);
    static std::wstring GetValue(const std::wstring& json, const std::wstring& path);

    static bool SetValueBool(const std::wstring& file_name, const std::wstring& path, const bool value);

    static bool JsonUtils::SetValueObj(const std::wstring& file_name, const std::wstring& json, bool is_add);

    static std::pair<std::wstring, std::wstring> GetNamesAndValues(const std::wstring& file_name, const std::wstring& path);
    static std::pair<std::wstring, std::wstring> GetNamesAndValues(const std::wstring& json);

    static std::wstring GetArrayValue(const std::wstring& json);

};